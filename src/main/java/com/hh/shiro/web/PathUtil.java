package com.hh.shiro.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class PathUtil {

	/**
	 * 当前的HTTPS地址转换到另一个HTTP地址
	 * @param request
	 * @param currentPath 当前访问路径(不包含应用名)。</br>
	 * 					如，当前访问地址是：http://shiro.local/shiro-spring-redis/auth/login
	 * 						当前应用是:shiro-spring-redis
	 * 						那么，currentPath的值应该为：/auth/login
	 * @param toPath 跳转的路径(不包含应用名)。如：/main/index
	 * @return toPath的HTTP地址
	 */
	public static String getWholePath(HttpServletRequest request, String currentPath, String toPath) {
		String requestUrl = request.getRequestURL().toString();
		String requestUri = request.getRequestURI().toString();
		String resUrl = StringUtils.replaceOnce(requestUrl, "https", "http");
		resUrl = StringUtils.substringBefore(resUrl, requestUri);
		String resUri = StringUtils.substringBefore(requestUri, currentPath);
		resUri = StringUtils.appendIfMissing(resUri, toPath);
		return String.format("%s%s", resUrl, resUri);
	}

}
