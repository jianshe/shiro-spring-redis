<#import "/common/htmlBase.ftl" as html/>
<@html.htmlBase cssFiles=[] jsFiles=["page/auth/registe.js",
									"common/jquery.validate.min.js"] localCssFiles=[] title="手机验证">
<div class="viewport">
		<div class="signin-container">
			<div class="login-input">
			<form action="${basepath!''}/auth/sms/validateSms" method="post" id="registeForm" data-ajax="false">
					<div class="login-top">
						<span class="phone-icon"></span>
						<input type="tel" placeholder="请输入手机号" data-role="none" name="mobilePhone" value="${mobilePhone!''}"/>
						<span class="input-end-icon"></span>
						<!-- 默认叉号，add class: default -->
						<!-- 红色叉号，add class: error -->
						<!-- 绿色对号，add class: success -->
						<span class="get-check-code disabled" id="getSmsBtn">验证码</span>
						<!-- 验证码可用状态，add class: enabled  -->
						<!-- 验证码不可用状态，add class: disabled  -->
						<!-- <span class="get-check-code disabled">45s后重发</span> -->
					</div>
					<div class="login-bottom">
						<span class="safe-icon"></span>
						<input type="tel" placeholder="请输入验证码" data-role="none" name="smsContent"/>
						<span class="input-end-icon default" name="deleteSmsBtn"></span>
					</div>
					<div class="error-tip"><!-- 没有错误提示时，其内容为空，但div的结构要保存 -->
						<span class="registeErrorMsg">${errorMsg!''}</span>
					</div>
			</div>
			<div class="login-btn-container">
				<div class="login-btn" id="smsValidateBtn" name="smsValidateBtn">下一步</div>
				<!--  可用登陆按钮，add class: enabled -->
			</div>
			</form>
			<div data-role="popup" id="popupBasic">
				<p>This is a completely basic popup, no options set.</p>
			</div>
		</div>
	</div>
</@html.htmlBase>

